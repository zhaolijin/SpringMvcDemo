<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title>Index</title>
</head>
<body>
    <h2>Hello!<c:if test="${not empty currentUser}">${currentUser.name}</c:if></h2>

    <div id="operate_log">
        <table border="1" align="center">
            <tr>
                <th>序号</th>
                <th>用户ID</th>
                <th>用户姓名</th>
                <th>系统模块</th>
                <th>调用方法</th>
                <th>响应数据</th>
                <th>IP地址</th>
                <th>执行时间</th>
                <th>创建时间</th>
            </tr>
            <c:forEach items="${operateLogList}" var="operateLog" varStatus="indexStatus">
               <tr>
                    <td>${operateLog.id}</td>
                    <td>${operateLog.userId}</td>
                    <td>${operateLog.userName}</td>
                    <td>${operateLog.module}</td>
                    <td>${operateLog.method}</td>
                    <td>${operateLog.responseData}</td>
                    <td>${operateLog.ipAddress}</td>
                    <td>${operateLog.executeTime}</td>
                    <td>${operateLog.createTime}</td>
               </tr>
            </c:forEach>
        </table>
    </div>
</body>
</html>