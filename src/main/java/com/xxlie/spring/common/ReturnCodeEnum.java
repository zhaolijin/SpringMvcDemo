package com.xxlie.spring.common;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Description
 * 100 - 继续
 * 200 - 请求成功
 * 301 - 资源（网页等）被永久转移到其它URL
 * 404 - 请求的资源（网页等）不存在
 * 500 - 内部服务器错误
 */
@Getter
@AllArgsConstructor
public enum ReturnCodeEnum {

    FAILED(0, "处理失败"),

    SUCCESS(1, "处理成功"),

    BAD_REQUEST(400, "坏的请求"),

    NOT_FOUND(400, "访问地址不存在"),

    SYSTEM_ERROR(500, "系统错误"),

    ;

    private int code;
    private String desc;
}
