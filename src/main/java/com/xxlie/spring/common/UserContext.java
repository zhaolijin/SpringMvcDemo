package com.xxlie.spring.common;

import com.xxlie.spring.entity.User;

/**
 * @author Credric
 * 登录上下文
 */
public class UserContext {

    private static ThreadLocal<User> threadLocal = new ThreadLocal<>();

    public static void setCurrentUser(User user) {
        threadLocal.set(user);
    }

    public static User getCurrentUser() {
        return threadLocal.get();
    }
}
