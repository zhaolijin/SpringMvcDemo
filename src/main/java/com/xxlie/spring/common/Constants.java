package com.xxlie.spring.common;

public class Constants {


    public static final String YES = "YES";

    public static final String NO = "NO";

    public static final String USER_SESSION_KEY = "USER_SESSION_KEY";

    public static final String ARE_NOT_LOGIN = "当前未登录";

    public static final String LOGIN_NAME_HAS_EXIST = "登录名已存在";

    public static final String LOGIN_NAME_IS_NOT_EXIST = "登录用户不存在";

    public static final String LOGIN_NAME_NOT_MATCH_PASSWD = "登录名和密码不匹配";
}
