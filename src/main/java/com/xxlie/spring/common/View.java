package com.xxlie.spring.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class View<T> implements Serializable {

    private int code;

    private String message;

    private T data;

    public View(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public View(int code, T data) {
        this.code = code;
        this.data = data;
    }

    public View(String message, T data) {
        this.message = message;
        this.data = data;
    }

    public static <T> View<T> ofOk() {
        return new View<>(ReturnCodeEnum.SUCCESS.getCode(), ReturnCodeEnum.SUCCESS.getDesc());
    }

    public static <T> View<T> ofOk(T data) {
        return new View<T>(ReturnCodeEnum.SUCCESS.getCode(), ReturnCodeEnum.SUCCESS.getDesc(), data);
    }

    public static <T> View<T> ofOk(String message, T data) {
        return new View<>(message, data);
    }

    public static <T> View<T> ofError() {
        return new View<>(ReturnCodeEnum.FAILED.getCode(), ReturnCodeEnum.FAILED.getDesc());
    }

    public static <T> View<T> ofError(String message) {
        return new View<>(ReturnCodeEnum.FAILED.getCode(), message);
    }

}
