package com.xxlie.spring.interceptor;

import com.xxlie.spring.annotation.CreateById;
import com.xxlie.spring.annotation.CreateByName;
import com.xxlie.spring.annotation.CreateTime;
import com.xxlie.spring.annotation.UpdateById;
import com.xxlie.spring.annotation.UpdateByName;
import com.xxlie.spring.annotation.UpdateTime;
import com.xxlie.spring.common.UserContext;
import com.xxlie.spring.entity.User;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlCommandType;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Invocation;
import org.apache.ibatis.plugin.Plugin;
import org.apache.ibatis.plugin.Signature;
import org.springframework.core.annotation.AnnotationUtils;

import java.lang.reflect.Field;
import java.util.Date;
import java.util.Objects;
import java.util.Properties;

/**
 * @author Cedric
 * 自动注入用户id,name以及自动获取注入创建时间和更新时间
 */
@Intercepts({@Signature(type = Executor.class, method = "update", args={MappedStatement.class, Object.class})})
public class AuditingInterceptor implements Interceptor {

    private Properties properties;

    @Override
    public Object intercept(Invocation invocation) throws Throwable {
        MappedStatement mappedStatement = (MappedStatement) invocation.getArgs()[0];
        SqlCommandType sqlCommandType = mappedStatement.getSqlCommandType();
        Object parameter = invocation.getArgs()[1];
        Field[] fields = parameter.getClass().getSuperclass().getDeclaredFields();
        Date currentDate = new Date();
        User currentUser = UserContext.getCurrentUser();
        if (SqlCommandType.INSERT == sqlCommandType) {
            for (Field field : fields) {
                if (AnnotationUtils.getAnnotation(field, CreateTime.class) != null) {
                    field.setAccessible(true);
                    field.set(parameter, currentDate);
                    field.setAccessible(false);
                }
                if (AnnotationUtils.getAnnotation(field, CreateById.class) != null) {
                    if (Objects.nonNull(currentUser)) {
                        field.setAccessible(true);
                        field.set(parameter, currentUser.getId());
                        field.setAccessible(false);
                    }
                }
                if (AnnotationUtils.getAnnotation(field, CreateByName.class) != null) {
                    if (Objects.nonNull(currentUser)) {
                        field.setAccessible(true);
                        field.set(parameter, currentUser.getName());
                        field.setAccessible(false);
                    }
                }
            }
            System.out.println(parameter.toString());
        } else if (SqlCommandType.UPDATE == sqlCommandType) {
            for (Field field : fields) {
                if (AnnotationUtils.getAnnotation(field, UpdateTime.class) != null) {
                    field.setAccessible(true);
                    field.set(parameter, currentDate);
                    field.setAccessible(false);
                }
                if (AnnotationUtils.getAnnotation(field, UpdateById.class) != null) {
                    if (Objects.nonNull(currentUser)) {
                        field.setAccessible(true);
                        field.set(parameter, currentUser.getId());
                        field.setAccessible(false);
                    }
                }
                if (AnnotationUtils.getAnnotation(field, UpdateByName.class) != null) {
                    if (Objects.nonNull(currentUser)) {
                        field.setAccessible(true);
                        field.set(parameter, currentUser.getName());
                        field.setAccessible(false);
                    }
                }
            }
            System.out.println(parameter.toString());
        }
        return invocation.proceed();
    }

    @Override
    public Object plugin(Object target) {
        if (target instanceof Executor) {
            return Plugin.wrap(target, this);
        } else {
            return target;
        }
    }

    @Override
    public void setProperties(Properties properties) {
        this.properties = properties;
        System.out.println("properties = " + this.properties);
    }
}
