package com.xxlie.spring.dao;

import com.xxlie.spring.entity.OperateLog;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OperateLogDao {

    Integer addOperateLog(OperateLog operateLog);

    OperateLog findById(Integer id);

    List<OperateLog> getOperateLogList();
}
