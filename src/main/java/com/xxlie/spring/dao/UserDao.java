package com.xxlie.spring.dao;

import com.xxlie.spring.entity.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDao {

    Integer addUser(User user);

    Integer updateById(User user);

    Integer deleteById(Integer id);

    User findById(Integer id);

    User findByName(String name);

    User findByAppid(String appid);

}
