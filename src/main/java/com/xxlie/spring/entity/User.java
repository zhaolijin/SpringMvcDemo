package com.xxlie.spring.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString(callSuper = true)
@NoArgsConstructor
public class User extends BaseEntity {

    private Integer id;

    private String name;

    private String passwd;

    private String appid;

}
