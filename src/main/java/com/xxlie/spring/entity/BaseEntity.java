package com.xxlie.spring.entity;

import com.xxlie.spring.annotation.CreateById;
import com.xxlie.spring.annotation.CreateByName;
import com.xxlie.spring.annotation.CreateTime;
import com.xxlie.spring.annotation.UpdateById;
import com.xxlie.spring.annotation.UpdateByName;
import com.xxlie.spring.annotation.UpdateTime;
import com.xxlie.spring.common.Constants;
import lombok.Data;

import java.util.Date;

@Data
public class BaseEntity {

    @CreateById
    private Integer createById;

    @CreateByName
    private String createByName;

    @UpdateById
    private Integer updateById;

    @UpdateByName
    private String updateByName;

    @CreateTime
    private Date createTime;

    @UpdateTime
    private Date updateTime;

    private String isDelete = Constants.NO;

}
