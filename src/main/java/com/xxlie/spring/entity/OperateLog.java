package com.xxlie.spring.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString(callSuper = true)
@NoArgsConstructor
public class OperateLog extends BaseEntity {

    private Integer id;

    private Integer userId;

    private String userName;

    private String module;

    private String method;

    private String responseData;

    private String ipAddress;

    private long executeTime;

}
