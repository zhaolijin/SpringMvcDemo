package com.xxlie.spring.service;

import com.xxlie.spring.dao.OperateLogDao;
import com.xxlie.spring.entity.OperateLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OperateLogService {

    @Autowired
    private OperateLogDao operateLogDao;

    public OperateLog findById(Integer id){
        return operateLogDao.findById(id);
    }

    public void addOperateLog(OperateLog operateLog) {
        operateLogDao.addOperateLog(operateLog);
    }

    public List<OperateLog> getOperateLogList() {
        return operateLogDao.getOperateLogList();
    }
}
