package com.xxlie.spring.controller;

import com.xxlie.spring.common.UserContext;
import com.xxlie.spring.common.View;
import com.xxlie.spring.entity.User;
import com.xxlie.spring.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/user")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(value ="/{id}")
    public User get(@PathVariable Integer id){
        return userService.findById(id);
    }

    @RequestMapping(value = "/update")
    public View update(@RequestParam(name = "appid") String appid) {
        if (userService.update(appid) > 0) {
            return View.ofOk();
        }
        return View.ofError();
    }

    @RequestMapping(value ="/my")
    public View<User> myInfo() {
        return View.ofOk(UserContext.getCurrentUser());
    }
}
